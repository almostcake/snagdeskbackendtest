﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectsAPI.Entities
{
    public class Project: EntityBase
    {
        /// <summary>
        ///     Возвращает список всех статусов для данного проекта
        /// </summary>
        [InverseProperty(nameof(Status.Project))]
        public IEnumerable<Status> Statuses { get; set; }
    }
}
