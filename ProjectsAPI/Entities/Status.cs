﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectsAPI.Entities
{
    public class Status: EntityBase
    {
        /// <summary>
        ///     Id проекта, к которому привязан статус
        /// </summary>
        [Required]
        public Guid ProjectId { get; set; }

        /// <summary>
        ///     Проект, к которому привязан статус
        /// </summary>
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }

        /// <summary>
        ///     Возвращает список задач с данным статусом
        /// </summary>
        [InverseProperty(nameof(Goal.Status))]
        public IEnumerable<Goal> Goals { get; set; }
    }
}
