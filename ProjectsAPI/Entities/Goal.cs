﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectsAPI.Entities
{
    public class Goal: EntityBase
    {
        /// <summary>
        ///     Id статуса, в котором находится задача
        /// </summary>
        [Required]
        public Guid StatusId { get; set; }

        /// <summary>
        ///     Cтатус, в котором находится задача
        /// </summary>
        [ForeignKey(nameof(StatusId))]
        public Status Status { get; set; }
    }
}
