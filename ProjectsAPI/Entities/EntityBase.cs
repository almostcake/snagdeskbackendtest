﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsAPI.Entities
{
    /// <summary>
    ///     Представляет абстрактный класс, начальной сущности базы данных с Id-ключом типа Guid
    ///     и обязательным наименованием Name типа string
    /// </summary>
    public abstract class EntityBase
    {
        /// <summary>
        ///     Идентификатор записи в базе данных
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        ///     Наименование объекта данных
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
