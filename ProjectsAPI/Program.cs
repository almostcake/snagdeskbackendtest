﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace ProjectsAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                // Remove useless header - HTTP server will be hidden by reverse-proxy
                .UseKestrel(options => options.AddServerHeader=false)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
