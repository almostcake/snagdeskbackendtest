﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ProjectsAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {
        private readonly ApplicationDbContext db;

        public ApplicationDbContext Db => db;

        public ProjectsController(ApplicationDbContext context)
        {
            db = context;
        }

        /// <summary>
        ///     GET api/projects/{id}
        ///     -------------------
        ///     Возвращает список задач для проекта с заданным GUID-id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="statusName"></param>
        /// <returns>IActionResult</returns>
        [HttpGet("{id:guid:required}")]
        public IActionResult Get(Guid id, [FromQuery] string statusName)
        {
            // Если не предоставлены обязательные параметры, или они не соответствуют типу возвращает 400
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var statuses = Db.Statuses.Include(status => status.Goals).AsNoTracking().Where(status => status.ProjectId == id);

            // Если явно указано название статуса
            if (!string.IsNullOrEmpty(statusName))
            {
                statuses = statuses.Where(status => status.Name == statusName);
            }

            // Если ничего не найдено возвращает 404
            if (!statuses.Any())
            {
                return NotFound();
            }

            // Итоговый результат возвращает список задач в формате {Id, Name, Status}
            var result = statuses
                .SelectMany(status => status.Goals)
                .Select(x => new { Id = x.Id, Name = x.Name, Status = x.Status.Name })
                .ToList();

            // Если ничего не найдено возвращает 404
            if (!result.Any())
            {
                return NotFound();
            }

            // Возвращаем 200 и список задач
            return Ok(result);
        }
    }
}
