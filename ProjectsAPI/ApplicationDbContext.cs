﻿using Microsoft.EntityFrameworkCore;
using ProjectsAPI.Entities;

namespace ProjectsAPI
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Project> Projects { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Goal> Goals { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }
    }
}
