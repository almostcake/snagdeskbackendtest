# Projects application Web API

## Stack:
* .NET Core 1.1.0
* EntityFrameworkCore 1.1.0 with MS SQL Server Express 2014

## SDK:
* .NET Core 1.1.0 SDK

## Tools:
* VS2017
	* .NET Core Tooling 1.0.1 Preview 2
* MS SQL Server 2014 Management Studio

## Windows Runtime:
* open with VS2017
* restore packages
* rebuild solution
* launch

## Linux Runtime:
### guide: https://docs.microsoft.com/en-us/aspnet/core/publishing/linuxproduction